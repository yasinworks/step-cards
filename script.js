import VisitCardiologist from "./modules/VisitCardiologist.js";
import VisitDentist from "./modules/VisitDentist.js";
import VisitTherapist from "./modules/VisitTherapist.js";
import {Visit} from "./modules/Visit.js";
import Authorization from "./modules/Authorization.js";

let auth = new Authorization()
let general = new Visit()
let repeater

document.addEventListener('load', auth.checkLogin())
document.addEventListener('load', general.createOnPage())

function cardsCheck() {
    if (document.querySelector(".card")){
        document.querySelector(".nothing").style.display = "none"
    } else {
        document.querySelector(".nothing").style.display = "block"
    }
    repeater = setTimeout(cardsCheck, 100);
}



if (document.getElementById('logIn')) {
    let logInBtn = document.getElementById('logIn')
    logInBtn.addEventListener('click', () => {
        auth.openModal()

    })
}



if (document.getElementById('addCard')) {

    if (document.querySelector(".card-body")) {
        document.querySelector(".nothing").remove()
    }

    let createCard = document.getElementById('addCard')
    createCard.addEventListener("click", () => {

        general.chooseDoctor()

        const chooseDoc = document.getElementById("chooseDoc")

        let Dentist = new VisitDentist()
        let Cardiologist = new VisitCardiologist()
        let Therapist = new VisitTherapist()

        const choseVisit = () => {
            if (localStorage.getItem("chosenDoctor") === "Cardiologist") {
                localStorage.removeItem("chosenDoctor")
                Cardiologist.createModal()
            } else if (localStorage.getItem("chosenDoctor") === "Dentist") {
                localStorage.removeItem("chosenDoctor")
                Dentist.createModal()
            } else if (localStorage.getItem("chosenDoctor") === "Therapist") {
                localStorage.removeItem("chosenDoctor")
                Therapist.createModal()
            }
        }

        function validation(visitInfo) {
            let allInputs = [document.querySelector(".dropdown-toggle"), ...document.querySelectorAll(".form-control")]
            allInputs.forEach(input => {
                if (input.value === "" || input.textContent === "Necessity") {
                    input.classList.toggle("input-error")
                }
            })
            for (const [key, value] of Object.entries(visitInfo)) {
                if (value === "" || value === "Necessity") {
                    throw new Error("Not all fields are filled")
                }
            }
        }

        chooseDoc.addEventListener("click", () => {
            choseVisit()

            let submit = document.getElementById("createCard")
            let modalTitle = document.querySelector(".modal-title")

            submit.addEventListener("click", () => {

                let erroredInputs = document.querySelectorAll(".input-error")
                erroredInputs.forEach(input => {
                    input.classList.remove("input-error")
                })

                try {
                    if (modalTitle.textContent === "Cardiologist") {
                        Cardiologist.gatherInfo()
                        validation(Cardiologist)
                        Cardiologist.sendCard()
                        Cardiologist.closeModal()
                    } else if (modalTitle.textContent === "Dentist") {
                        Dentist.gatherInfo()
                        validation(Dentist)
                        Dentist.sendCard()
                        Dentist.closeModal()
                    } else {
                        Therapist.gatherInfo()
                        validation(Therapist)
                        Therapist.sendCard()
                        Therapist.closeModal()
                    }
                    general.createOnPage()
                } catch (err) {
                    console.error(err.message);
                }
            })
        })
    })

}

cardsCheck();
