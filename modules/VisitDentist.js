import {Visit, Modal} from "./Visit.js";

class VisitDentist extends Visit{
    constructor() {
        super()
        this.doctor = "Dentist"
    }

    createModal() {
        super.createModal();
        localStorage.removeItem("chosenDoctor")
        this.modal.addTitle("Dentist")
        this.modal.addInput('dd-mm-yyy' ,'Date of Last Visit',false ,"date")
    }
    gatherInfo() {
        super.gatherInfo();
        this.date = document.getElementById("date").value
    }

}

export default VisitDentist